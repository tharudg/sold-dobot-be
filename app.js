const server = require('./server');
const imgRoutes = require('./routes/imgRoutes');

server.router.post('/scanned/image', imgRoutes.captureImage);

server.router.post('/points', (h)=>{
    let params = h.payload;
    return {
      client_message: params
    }
})

server.start();